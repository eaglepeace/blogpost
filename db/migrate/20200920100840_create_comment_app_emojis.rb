class CreateCommentAppEmojis < ActiveRecord::Migration[6.0]
  def change
    create_table :comment_app_emojis do |t|
      t.bigint :user_id
      t.bigint :comment_id
      t.bigint :app_emoji_id
      t.index ["app_emoji_id"], name: "index_comment_app_emojis_on_app_emoji_id"
      t.index ["comment_id"], name: "index_comment_app_emojis_on_comment_id"
      t.index ["user_id", "comment_id", "app_emoji_id"], name: "comment_app_emoji_index", unique: true
      t.index ["user_id"], name: "index_comment_app_emojis_on_user_id"

      t.timestamps
    end
  end
end
