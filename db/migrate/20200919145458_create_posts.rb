class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.string :title
      t.integer :status, default: 0
      t.date :publish_at
      t.bigint :user_id
      t.string :slug
      t.index ["user_id"], name: "index_posts_on_user_id"

      t.timestamps
    end
  end
end
