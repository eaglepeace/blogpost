class CreateAppEmojis < ActiveRecord::Migration[6.0]
  def change
    create_table :app_emojis do |t|
      t.string :name

      t.timestamps
    end
  end
end
