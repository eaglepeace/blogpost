class Comments::CancelsController < AdminController
  def show
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:comment_id])
  end
end