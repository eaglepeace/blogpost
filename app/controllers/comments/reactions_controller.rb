class Comments::ReactionsController < AdminController
  before_action :set_comment
  before_action :set_emoji

  def update
    emojis = @comment.comment_app_emojis.find_by(user: current_user, app_emoji: @app_emoji)
    @comment.comment_app_emojis.create(user: current_user, app_emoji: @app_emoji) unless emojis
  end

  def destroy
    emojis = @comment.comment_app_emojis.find_by(user: current_user, app_emoji: @app_emoji)
    @comment.comment_app_emojis.delete(emojis) if emojis
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_emoji
      @app_emoji = AppEmoji.find(params[:emoji])
    end
end
