class PostsController < ApplicationController
  def index
     # @posts = Post.drafted
    @posts = Post.published
    @users = User.order(:email)
  end

  def show
    @post = Post.find(params[:id])
  end
end