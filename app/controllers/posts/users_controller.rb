class Posts::UsersController < ApplicationController
  before_action :set_user

  def index
    @posts = @user.posts.includes(:comments)
  end

  private
    def set_user
      @user = User.find_by!(email: params[:username])
    end
end