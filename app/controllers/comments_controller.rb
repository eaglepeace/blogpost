class CommentsController < AdminController
  before_action :set_post
  before_action :set_comment, only: [:edit, :update]

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.post = @post

    @comment.save!
  end

  def destroy
    # We can delete any comment if we are the post owner
    # else we can only delete our own posts

    @comment = Comment.find(params[:id])

    if @comment.destroyable?(current_user)
      @comment.delete
    else
      # It's a situation that shouldn't happen unless there is a bug or someone is messing with the HTTP request
      # we can ignore or not ... like raise an exception ...
    end
  end

  def edit
  end

  def update
    if @comment.editable?(current_user)
      @comment.update(comment_params)
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:content)
    end

    def set_comment
      @comment = @post.comments.find(params[:id])
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

  
end