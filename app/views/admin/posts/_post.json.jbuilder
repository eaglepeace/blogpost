json.extract! post, :id, :title, :status, :publish_at, :created_at, :updated_at
json.url post_url(post, format: :json)
