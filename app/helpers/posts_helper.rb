module PostsHelper


  def posts_number_by_email(user)
    user.email  +  " " + pluralize(user.posts.count, "post", "posts")
  end
end
