module ApplicationHelper
  def emoji_link(comment, app_emoji)
    comment_with_app_emoji = comment.comment_app_emojis.find_by(user: current_user, app_emoji: app_emoji)
    url = ""
    method = ""

    url = if comment_with_app_emoji
      method = :delete
      reaction_path(comment, emoji: app_emoji)
    else
      method = :patch
      reaction_path(comment, emoji: app_emoji)
    end

    first = link_to url, remote: true, method: method do
      image_tag(app_emoji.name.image_url, height: "32", width: "32")
    end

    second = tag.div(id: "comment_#{comment.id}_emoji_#{app_emoji.id}") do
      (comment_with_app_emoji ? " selected" : "")
    end

    first + second
  end

  def display_emojis(comment)
    emojis = AppEmoji.where(id: comment.comment_app_emojis.select(:app_emoji_id).distinct)
    emojis_html = emojis.map do |emoji|
      reactions = comment.comment_app_emojis.where(comment: comment)
      reactions_nr = emoji.comment_app_emojis.where(comment: comment).count
      reactions_users = reactions.map{ |reaction| reaction.user.email }.join(",")
      image_tag(emoji.name.image_url, height: "32", width: "32", title: reactions_users )  + " (#{reactions_nr }) "
      
    end
      

    safe_join(emojis_html)
  end
end
