class CommentAppEmoji < ApplicationRecord
  belongs_to :user
  belongs_to :comment
  belongs_to :app_emoji
  

  # same user, same comment but different emoji
  validates :user_id, uniqueness: {scope: [:comment_id, :app_emoji_id] } 
end
