class Post < ApplicationRecord
  include Posts::PostStateFinders

  enum status: { draft: 0, published: 1 }

  has_rich_text :content

  belongs_to :user
  has_many :comments

  validates :title, presence: true
  validates :content, presence: true
  validates :publish_at, presence: true
  validates :slug, presence: true

  delegate :email, to: :user




  
  before_validation :set_slug

  private
    def set_slug
      self.slug = title.downcase.parameterize
    end
end
