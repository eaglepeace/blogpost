class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post
  has_many :comment_app_emojis

  delegate :email, to: :user

  has_rich_text :content

  def editable?(user)
    self.user == user
  end

  # Other options, perhaps better than this one:
  #   - use a policy pattern (like pundit gem)
  #   - the model know if its editable, but to avoid sending current_user, I would for for a Current.user
  #     (ActiveSupport::CurrentAttributes)
  def destroyable?(user)
    self.user == user || post.user == user
  end
end
