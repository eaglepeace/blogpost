class AppEmoji < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  has_many :comment_app_emojis
end
