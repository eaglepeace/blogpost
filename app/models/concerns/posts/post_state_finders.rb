module Posts::PostStateFinders
  extend ActiveSupport::Concern

  class_methods do
  #  a field like :title, modality like "ASC" or "DESC"
  # Post.ordered_by(:title,"ASC")
    def ordered_by(field,modality)
      order("#{field} #{modality}")
    end

    def published
      where(["status = ?", :published])
    end

    def drafted
      where(["status = ?", :draft])
    end

    # find posts by email and status 0(draft) or 1(published)
    # Post.created_by("email@gmail.com",1)
    def created_by(email_user,status)
      joins(:user).where(["email = ? AND status = ?", email_user,status])
    end

    #find posts by searching the title
    #Post.search_by("pippo")
    def search_by(text)
      where(["title LIKE ?", "#{text}"])
    end
  end 
  
end