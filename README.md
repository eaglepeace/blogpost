

# Development envirment

If you're cloning the code for the first time, rreate a config/database.yml file:

```
$ cp config/database.mysql.yml config/database.yml
```

Then:

```
$ bin/bundle install
$ bin/yarn install
$ bin/rails db:create
$ bin/rails db:schema:load
$ bin/rails db:seed
$ bin/rails s
