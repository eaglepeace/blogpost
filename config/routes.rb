Rails.application.routes.draw do
  
  devise_for :users
  root to: "posts#index"

  resources :posts, only: [:index] do
    resources :comments do
      resource :cancel, only: [:show], module: :comments
      resources :reactions, only: [:destroy, :update], module: :comments, shallow: true
    end
  end

  namespace :admin do
    resources :posts
  end

  get "posts/:username", to: "posts/users#index", as: "post_users", constraints: { username: /.*/ }
  get ":id/(:slug)", to: "posts#show", as: "post"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
