require 'gemojione/string_ext'
Gemojione.asset_path = '/assets/emoji/svg'
Gemojione.default_size = '64px'
Gemojione.use_svg = true
Gemojione.use_sprite = true