ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  
  # Capybara test
  def log_in_with(email, password)
    visit new_user_session_path

    fill_in 'user_email', with: email
    fill_in 'user_password', with: password

    click_button 'Log in'
  end
end

